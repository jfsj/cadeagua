import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LackPage } from './lack';

@NgModule({
  declarations: [
    LackPage,
  ],
  imports: [
    IonicPageModule.forChild(LackPage),
  ],
})
export class LackPageModule {}
